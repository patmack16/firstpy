Program Breakdown: 
The user first needs to call numgen file and enter in a desired output filename, an array size, and(if desired) a random number range.  

After running numgen, the user then calls main.py program where they are prompted to enter the filename from either Numgen or a different file they would like sorted.  

Main then proceeds to read in the file and put each line as a separate element in a list.  

Quicksort is then called from the qs.py file.  This is a straight Python implementation from Cormen's book. (No added flair here) 

The now sorted array is then written to a new txt file with the same name as the original but having "-sorted" appended to the file name.  

Lastly, The user is informed of how many values were sorted as well as a readout of the sorted values.
